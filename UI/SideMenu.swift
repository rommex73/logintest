//
//  SideMenu.swift
//  1Temiz
//
//  Created by Roman Medvid on 6/6/18.
//  Copyright © 2018 Air. All rights reserved.
//

import Foundation
import UIKit

class MenuCell: UITableViewCell {
    
}

class SideMenu : NSObject, UITableViewDelegate, UITableViewDataSource  {
    
    private static let MENU_PADDING = 16
    private var menuWidth: CGFloat?
    private var menuContainer = UIView()
    private var isMovedAway = false
    
    private var menuTableView: UITableView!
    private var scale: CGFloat!
    
    private let myArray = ["First", "Second", "Third"]
    
    func setSizeAndPosition (basedOn view: UIView) {
        
        scale = view.contentScaleFactor
        
        menuWidth = view.bounds.width * 0.60
        menuContainer.frame = CGRect (x: 0, y: 0, width: menuWidth!, height: view.bounds.height)
        menuContainer.backgroundColor = UIColor (white: 0.9, alpha: 0.9)
        view.addSubview(menuContainer)
        menuContainer.transform = CGAffineTransform (translationX: -menuWidth!/scale, y: 0)
        isMovedAway = false
        
        setupTable()
    }
    
    func setupTable () {
        menuTableView = UITableView(frame: CGRect(x: CGFloat (SideMenu.MENU_PADDING ), y: CGFloat ( SideMenu.MENU_PADDING),
                                                width: menuWidth! - CGFloat (SideMenu.MENU_PADDING * 2),
                                                height: menuContainer.frame.height - CGFloat (SideMenu.MENU_PADDING * 2)) )
        menuTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        menuTableView.dataSource = self
        menuTableView.delegate = self
        menuContainer.addSubview (menuTableView)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Num: \(indexPath.row)")
        print("Value: \(myArray[indexPath.row])")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.textLabel!.text = "\(myArray[indexPath.row])"
        return cell
    }
    
    func show() {
        if !isMovedAway { return }
        UIView.animate(withDuration: 0.3) {
            self.menuContainer.transform = CGAffineTransform (translationX: self.menuWidth!/self.scale, y: 0)
        }
        isMovedAway = false
        
    }
    
    func hide() {
        if isMovedAway { return }
        UIView.animate(withDuration: 0.3) {
            self.menuContainer.transform = CGAffineTransform (translationX: -self.menuWidth!/self.scale, y: 0)
        }
        isMovedAway = true
    }
    
    func toggle () {
        if isMovedAway {
            show()
        } else {
            hide()
        }
    }
}

class SideMenuFactory {
    static func getMenu(rootView: UIView) -> SideMenu {
        var newMenu = SideMenu()
        newMenu.setSizeAndPosition (basedOn: rootView)
        return newMenu
    }
}
