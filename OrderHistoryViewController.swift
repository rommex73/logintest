//
//  OrderHistoryViewController.swift
//  LoginTest
//
//  Created by Air on 6/6/18.
//  Copyright © 2018 Air. All rights reserved.
//

import UIKit


class OrderHistoryItem: UITableViewCell {
    
    @IBOutlet weak var dryCleanerImage: UIImageView!
    @IBOutlet weak var dryCleanerName: UILabel!
    @IBOutlet weak var viewOrderButton: UIButton!
}

class OrderHistoryItemSelected: OrderHistoryItem {
    @IBAction func rateItButton(_ sender: UIButton) {
    }
    @IBAction func viewOrderButton(_ sender: UIButton) {
    }
}

class OrderHistoryViewController: UIViewController {
    
    @IBOutlet weak var whiteContainer: UIView!
    @IBOutlet weak var screenTitle: UILabel!
    
    @IBOutlet weak var ordersTableView: UITableView!
    
    typealias OrdersHistoryEntry = (name: String, image: UIImage, isOpen: Bool)
    private var ordersHistoryList: [OrdersHistoryEntry] = []
    
    private var selectedRow: Int = -1
    
    var menu: SideMenu?
    
    var getOrderListTask: URLSessionDataTask?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        whiteContainer.layer.cornerRadius = 19
        screenTitle.text = "ORDER HISTORY"
        
        ordersTableView.delegate = self
        ordersTableView.dataSource = self
        
        ordersHistoryList.append(  (name: "string1", image: UIImage (named: "envelope")!, isOpen: false ) )
        ordersHistoryList.append(  (name: "string2", image: UIImage (named: "password")!, isOpen: false ) )
        
        getOrderListAttempt (with: "erhan@flepron.com", andTicket: "TICKET")
        
        menu = SideMenuFactory.getMenu (rootView: self.view)
        
    }
    
    @IBAction func menuButtonPressed(_ sender: UIButton) {
        menu?.toggle()
    }
    
    
    func getOrderListAttempt(with email: String, andTicket: String) {
        // prepare json data
        let json: [String: Any] = ["UserID": email,
                                   "Ticket": andTicket,
                                   "OnlyCompleted": true]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        // create post request
        let url = URL(string: "https://beta-api.1temiz.com/Order.svc/GetOrderList")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        
        getOrderListTask = URLSession.shared.dataTask(with: request) { data, response, error in
//            DispatchQueue.main.async {
//                self.progressCircle.isHidden = true
//                self.progressCircle.stopAnimating()
//            }
            
            
            guard let data = data, error == nil else {
                //print(error?.localizedDescription ?? "No data")
                //self.showTechMessage ("No response. Try again later")
                return
            }
            
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
                var responseJSONMockUp = JSONMockUpResponse.orderHistoryList
            } else {
                //self.showTechMessage ("Failed response. Try again later")
            }
        }
        
        getOrderListTask!.resume()
    }
}

//MARK: UITableViewDataSource protocol
extension OrderHistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ordersHistoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let index = indexPath.row
        
        var cell: UITableViewCell
        if ordersHistoryList[index].isOpen {
            cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryItemSelected", for: indexPath) as! OrderHistoryItemSelected } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryItem", for: indexPath) as! OrderHistoryItem }
    
        var cellTrue: OrderHistoryItem = cell as! OrderHistoryItem
        cellTrue.dryCleanerName.text = ordersHistoryList[index].name
        cellTrue.dryCleanerImage.image = ordersHistoryList[index].image
        
//        if ordersHistoryList[index].isOpen {
//            cell.frame = CGRect (x: 0, y: 0, width: cell.bounds.width, height: cell.bounds.height + 40)
//        }
        
        //            if index == dbxLUTSelectedPosition {
        //                cell.backgroundColor =    UIColor.blue
        //                cell.dbxName.textColor = UIColor.white
        //            } else {
        //                cell.backgroundColor =    UIColor.white
        //                cell.dbxName.textColor = UIColor.black
        //            }
        return cellTrue
    }
}

//MARK: UITableViewDelegate protocol
extension  OrderHistoryViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let index = indexPath.row
        if index == selectedRow  {
            guard selectedRow != -1 else { return }
            selectedRow = -1
            ordersHistoryList[index].isOpen = false
            ordersTableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        } else {
            if selectedRow > -1 {
                ordersHistoryList[selectedRow].isOpen = false
                ordersTableView.reloadRows(at: [ IndexPath (item: selectedRow, section: 0) ], with: UITableViewRowAnimation.fade)
            }
            selectedRow = index
            ordersHistoryList[selectedRow].isOpen = true
            
            ordersTableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        }
        ordersTableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let index = indexPath.row
        if index == selectedRow  {
            return 124
        } else {
            return 64
        }
    }
    
    func tableView(_ tableView: UITableView,
                   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let index = indexPath.row
        if index == selectedRow  {
            return 124
        } else {
            return 64
        }
    }
}
