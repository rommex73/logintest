//
//  String+.swift
//  LoginTest
//
//  Created by Air on 5/16/18.
//  Copyright © 2018 Air. All rights reserved.
//

import Foundation

// https://stackoverflow.com/questions/25471114/how-to-validate-an-e-mail-address-in-swift

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$"
        return NSPredicate(format:"SELF MATCHES %@", emailRegEx).evaluate(with: self)
    }
}
