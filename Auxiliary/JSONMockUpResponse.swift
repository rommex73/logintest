//
//  JSONMockUpResponse.swift
//  1Temiz
//
//  Created by Air on 6/7/18.
//  Copyright © 2018 Air. All rights reserved.
//

import Foundation

class JSONMockUpResponse {
    static public var orderHistoryList: String? {
        get {
            var s: String?
            try?  s = String (contentsOf: URL (fileURLWithPath: "Auxiliary/fileResponse.txt"))
            return s
        }
    }
}
