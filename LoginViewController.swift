//
//  ViewController.swift
//  LoginTest
//
//  Created by Air on 5/16/18.
//  Copyright © 2018 Air. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    
    var loginTask: URLSessionDataTask?
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var progressCircle: UIActivityIndicatorView!
    @IBOutlet weak var techMessageLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.layer.cornerRadius = 19
        loginButton.layer.masksToBounds = true
        
        progressCircle.isHidden = true
        progressCircle.stopAnimating()
    }
    
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        techMessageLabel.isHidden = true
        
        
        let email = emailTextField.text
        let password = passwordTextField.text
        
        //        guard email != nil else {
        //            failTextField (emailTextField)
        //            return
        //        }
        //
        //        guard password != nil else {
        //            failTextField (passwordTextField)
        //            return
        //        }
        //
        //        guard email!.isValidEmail() else {
        //            failTextField (emailTextField)
        //            return
        //        }
        //
        //        guard password!.count > 5 && password!.count < 61 else {
        //            failTextField (passwordTextField)
        //            return
        //        }
        
        self.view.endEditing(true) // hide keyboard
        
        progressCircle.isHidden = false
        progressCircle.startAnimating()
        
        
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
        //        let segueID = "LoginToOrderHistorySegue"
        //        performSegue(withIdentifier: segueID, sender: nil)
        
        loginAttempt(with: "erhan@flepron.com", and: "123456")
        
        
        
    }
    
    func failTextField (_ site: UIView) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.1,
                           animations: { site.backgroundColor = UIColor (red: 1, green: 0.5, blue: 0.5, alpha: 0.5) },
                           completion: {_ in
                            UIView.animate(withDuration: 0.1) {
                                site.backgroundColor = UIColor.clear
                            }
            })
        }
        loginButton.shake()
    }
    
    func loginAttempt(with email: String, and password: String) {
        // prepare json data
        let json: [String: Any] = ["UserID": email,
                                   "Password": password,
                                   "Platform": 20]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        // create post request
        let url = URL(string: "https://beta-api.1temiz.com/Membership.svc/Login")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        
        loginTask = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                self.progressCircle.isHidden = true
                self.progressCircle.stopAnimating()
            }
            
            
            guard let data = data, error == nil else {
                //print(error?.localizedDescription ?? "No data")
                self.showTechMessage ("No response. Try again later")
                return
            }
            
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            
            let s_initial = responseJSON as? String
            
            
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
                self.successResponse(responseJSON)
            } else {
                self.showTechMessage ("Failed response. Try again later")
            }
        }
        
        loginTask!.resume()
    }
    
    
    func successResponse (_ response: [String: Any]) {
        if let code = response["StatusCode"] as? Int {
            if code == 100 {
                showTechMessage("Successful login!")
                
                if let text = response["StatusText"] as? String {
                    print(text)
                }
                print (response["Data"]!)
                
                
                
                let d = response["Data"]! as? [String:Any]
                let s = response["Data"]! as? String
                print (s)
                
                if let data = response["Data"] as? NSDictionary {
                    print(data)
                }
                
                if let nestedDictionary = response["Data"] as? [String: Any] {
                    print(nestedDictionary)
                }
                
                if let nestedArray = response["Data"] as? NSArray {
                    print(nestedArray)
                }
                
                
            } else {
                self.showTechMessage ("Failed response. Try again later")
            }
            
            let segueID = "LoginToOrderHistorySegue"
            performSegue(withIdentifier: segueID, sender: nil)
            return
            
            
        } else {
            if let status = (response["StatusText"]){
                showTechMessage(String (describing: status)  )
            }
        }
    }




func showTechMessage (_ message: String) {
    
    DispatchQueue.main.async {
        self.techMessageLabel.isHidden = false
        self.techMessageLabel.alpha = 0
        self.techMessageLabel.text = message
        UIView.animate(withDuration: 0.5) {
            self.techMessageLabel.alpha = 1
        }
    }
}


}

